#import "Specs.h"
#import "KDMContact.h"
#import <CoreData+MagicalRecord.h>
SPEC_BEGIN(KDMContactSpec)
describe(@"Tests on KDMContact", ^{
  __block KDMContact *contact;
  [MagicalRecord setupCoreDataStackWithStoreNamed:@"Model.sqlite"];
  [KDMContact MR_truncateAll];
  [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveOnlySelfAndWait];
  
  
  describe(@"CRUD operations on KDMContact", ^{
    beforeEach(^{
      contact = [KDMContact MR_createEntity];
    });
    
    it(@"should save contact object", ^{
      [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveOnlySelfAndWait];
      KDMContact *contactLocal = [KDMContact MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"name == %@",nil]];
      expect(contactLocal.name).beNil;
    });
    
    it(@"should set contact name", ^{
      contact = [KDMContact MR_createEntity];
      contact.name = @"imie";
      [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveOnlySelfAndWait];
      KDMContact *contactLocal = [KDMContact MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"name == %@",@"imie"]];
      expect(contactLocal.name).to.equal(@"imie");
      
    });
    
    it(@"should set contact image url", ^{
      contact = [KDMContact MR_createEntity];
      contact.image = @"www.wp.pl";
      [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveOnlySelfAndWait];
      KDMContact *contactLocal = [KDMContact MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"image == %@",@"www.wp.pl"]];
      expect(contactLocal.image).to.equal(@"www.wp.pl");
    });
    
    it(@"should update contact name", ^{
      KDMContact *contactLocal = [KDMContact MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"name == %@",@"imie"]];
      expect(contactLocal.name).to.equal(@"imie");
      contactLocal.name = @"imie2";
      [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveOnlySelfAndWait];
      contactLocal = [KDMContact MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"name == %@",@"imie2"]];
      contactLocal.name = @"imie2";
      expect(contactLocal.name).to.equal(@"imie2");

    });

    
  });
  
});
SPEC_END