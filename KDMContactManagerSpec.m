#import "Specs.h"
#import "KDMContactManager.h"
SPEC_BEGIN(KDMContactManagerSpec)
describe(@"KDMContactManagerSpec spec", ^{
  [Expecta setAsynchronousTestTimeout:5];
  
  __block KDMContactManager *manager;
  
  beforeEach(^{
    manager = [KDMContactManager new];
  });
  
  it(@"should return 3 dto contacts", ^{
    [manager refresh];
    expect(manager.contacts.count).will.equal(3);

  });
  
});
SPEC_END

