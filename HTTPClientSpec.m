#import "Specs.h"
#import "KDMHTTPClient.h"

SPEC_BEGIN(HTTPClientSpec)
describe(@"Tests on KDMHTTPClient, to increase test speed shound be done using OCMock", ^{
  [Expecta setAsynchronousTestTimeout:3];
  __block KDMHTTPClient *httpClient;
  
  describe(@"Geting instannce of httpclient", ^{
    httpClient  = [KDMHTTPClient sharedClient];
  });
  
  it(@"httpClient should not be nil", ^{
    expect(!httpClient).beNil;
  });
  
  describe(@"Downloadind data from server", ^{
    
    __block ResultBlock resultBlock;
    __block ClientResponseCode resultStatus;
    __block BOOL isDictionary ;
    __block BOOL hasContactsObject;
    __block NSUInteger contactsCount;
    
    beforeEach(^{
      resultStatus = ClientResponseCodeNoData;
      isDictionary = NO;
      hasContactsObject = NO;
      contactsCount = 0;
      
      resultBlock = ^(ClientResponseCode code, NSDictionary *responseData){
        resultStatus = code;
        if([responseData isKindOfClass:[NSDictionary class]]){
          isDictionary = YES;
          hasContactsObject = !!responseData[@"contacts"];
          if(hasContactsObject)
            contactsCount = [responseData[@"contacts"] count];
        }
        
      };

    });
    it(@"should download data witch success code", ^{
      [httpClient sendGETRequst:kServerURL withResultBlock:resultBlock];
      expect(resultStatus).will.equal(ClientResponseCodeSucces);
    });
    
    it(@"has dictionary", ^{
      [httpClient sendGETRequst:kServerURL withResultBlock:resultBlock];
      expect(isDictionary).will.beTruthy;
    });
    
    it(@"has contacts array", ^{
      expect(hasContactsObject).will.beTruthy;
    });
    
    it(@"has 3 contacts", ^{
      expect(contactsCount).will.equal(3);
    });
  });

  
});
SPEC_END

