#import "Specs.h"
#import "KDMContactDTO.h"

SPEC_BEGIN(KDMContactDTOSpec)
describe(@"Tests on KDMContactDTO", ^{
  __block KDMContactDTO *contactDTO;
  beforeEach(^{
    contactDTO = [KDMContactDTO new];
  });
  
  it(@"should init contact dto", ^{
    expect(!contactDTO).beNil;
  });
  
  it(@"should set name", ^{
    contactDTO.name = @"name";
    expect(contactDTO.name).to.equal(@"name");
  });
  
  it(@"should set avatar url", ^{
    contactDTO.avatarURL = @"www.google.pl";
    expect(contactDTO.avatarURL).to.equal(@"www.google.pl");
  });
  
});
SPEC_END

