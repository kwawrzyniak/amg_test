//
//  main.m
//  template
//
//  Created by Karol Wawrzyniak on 17.03.2014.
//  Copyright (c) 2014 Kadomi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Cedar-iOS/Cedar-iOS.h>

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, @"CedarApplicationDelegate");
    }
}
