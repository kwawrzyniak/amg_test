//
//  KBFErrorHandler.h
//  kbf
//
//  Created by Karol Wawrzyniak on 10.12.2013.
//  Copyright (c) 2013 Likims. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MBErrorHandler : NSObject
+(void)handleError:(id)obj;
+(void)handleErrorWithOkAction:(void (^)(void))action andError:(id)obj;
+(void)handleErrorWithOkAction:(void (^)(void))action andCancelAction:(void (^)(void))cancelAction andError:(id)obj;
@end
