//
//  KDMConst.h
//  Contacts
//
//  Created by Karol Wawrzyniak on 17.03.2014.
//  Copyright (c) 2014 Kadomi. All rights reserved.
//

#ifndef amg_KDMConst_h
#define amg_KDMConst_h

enum{
  ClientResponseCodeSucces = 0,
  ClientResponseCodeError = 1,
  ClientResponseCodeNoData = 2
}typedef ClientResponseCode;

typedef void (^ResultBlock)(ClientResponseCode code, id obj);

#define kServerURL @"http://apricotsoftware.pl/~herq/datasource.json"

#endif
