//
//  KDMContact.m
//  Contacts
//
//  Created by Karol Wawrzyniak on 17.03.2014.
//  Copyright (c) 2014 Kadomi. All rights reserved.
//

#import "KDMContact.h"


@implementation KDMContact

@dynamic id_;
@dynamic name;
@dynamic image;

@end
