//
//  KDMContactManager.m
//  Contacts
//
//  Created by Karol Wawrzyniak on 17.03.2014.
//  Copyright (c) 2014 Kadomi. All rights reserved.
//

#import "KDMContactManager.h"
#import "KDMContactDTO.h"
#import "KDMContact.h"
#import "KDMHTTPClient.h"
#import "NSError+AMGError.h"
#import "NSManagedObject+Init.h"
#import <CoreData+MagicalRecord.h>
@interface KDMContactManager ()
@property (nonatomic, strong) KDMHTTPClient *httpClient;
@end
@implementation KDMContactManager

- (instancetype)init
{
  self = [super init];
  if (self) {
    self.httpClient = [KDMHTTPClient sharedClient];
  }
  return self;
}
-(void)refresh{
  __weak KDMContactManager *_self = self;
  
  [self.httpClient sendGETRequst:kServerURL withResultBlock:^(ClientResponseCode code, id obj) {
    if (code == ClientResponseCodeSucces) {
      [_self  parseServerResposne:obj];
    }else{
      if ([_self.contactDelegate respondsToSelector:@selector(didFailToGetContacts:)]) {
        [_self.contactDelegate didFailToGetContacts:obj];
      }
    }
  }];
}
-(void)parseServerResposne:(id)obj{
  if (![obj isKindOfClass:[NSDictionary class]]) {
    NSError *error = [NSError errorWithLocalizedMessage:NSLocalizedString(@"Wrong server response fromat", nil)];
    if ([self.contactDelegate respondsToSelector:@selector(didFailToGetContacts:)]) {
      [self.contactDelegate didFailToGetContacts:error];
    }
    return;
  }
  NSArray *contacts = obj[@"contacts"];
  if (!contacts) {
    NSError *error = [NSError errorWithLocalizedMessage:NSLocalizedString(@"No contacts in server response", nil)];
    if ([self.contactDelegate respondsToSelector:@selector(didFailToGetContacts:)]) {
      [self.contactDelegate didFailToGetContacts:error];
    }
    return;
  }
  [KDMContact MR_truncateAll];
  
  for (NSDictionary *contactDic in contacts) {
    [KDMContact initWithDictionary:contactDic];
  }

  [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
    if (success) {

      NSArray *contactsDTOs = [self contacts];
      
      if ([self.contactDelegate respondsToSelector:@selector(didGetContacts:)]) {
        [self.contactDelegate didGetContacts:contactsDTOs];
      }
      
    }else{
      
      if ([self.contactDelegate respondsToSelector:@selector(didFailToGetContacts:)]) {
        [self.contactDelegate didFailToGetContacts:error];
      }
      
    }
  }];
  
}
-(NSArray *)contacts{
  NSArray *contacts = [KDMContact MR_findAll];
  NSArray *contactsDTOs = [self createDTOs:contacts];
  return contactsDTOs;
}
-(NSArray*)createDTOs:(NSArray*)contactsObjects{
  NSMutableArray *contactsDTOs = [NSMutableArray array];
  for (KDMContact *contact in contactsObjects) {
    KDMContactDTO *contactDTO = [KDMContactDTO new];
    contactDTO.name = contact.name;
    contactDTO.avatarURL = contact.image;
    [contactsDTOs addObject:contactDTO];
  }
  return contactsDTOs;
}
-(void)dealloc{
  self.contactDelegate = nil;
}
@end
