//
//  KDMContactManager.h
//  Contacts
//
//  Created by Karol Wawrzyniak on 17.03.2014.
//  Copyright (c) 2014 Kadomi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KDMContactDTO.h"

@protocol KDMContactManagerDelegate<NSObject>
@optional

-(void)didGetContacts:(NSArray*)contacts;
-(void)didFailToGetContacts:(NSError*)error;

@end
@interface KDMContactManager : NSObject
@property (nonatomic, readonly) NSArray *contacts;
@property (nonatomic, weak) id<KDMContactManagerDelegate>contactDelegate;
-(void)refresh;
@end
