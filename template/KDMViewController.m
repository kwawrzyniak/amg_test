//
//  KDMViewController.m
//  template
//
//  Created by Karol Wawrzyniak on 17.03.2014.
//  Copyright (c) 2014 Kadomi. All rights reserved.
//

#import "KDMViewController.h"
#import "KDMContactManager.h"
#import "KDMContactTableViewCell.h"
#import "MBErrorHandler.h"
#import "KDMContactDetailsViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>
@interface KDMViewController ()<KDMContactManagerDelegate,UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) KDMContactManager *manager;
@property (weak, nonatomic) IBOutlet UITableView *contactsTableView;
@property (nonatomic, strong) NSArray *datasource;
@end

@implementation KDMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
  self.manager = [KDMContactManager new];
  self.manager.contactDelegate = self;
  self.edgesForExtendedLayout = UIRectEdgeNone;
  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
  [self.manager refresh];
  
	// Do any additional setup after loading the view, typically from a nib.
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
  if ([segue.identifier isEqualToString:@"PushContactDetails"]) {
    KDMContactDetailsViewController *details = [segue destinationViewController];
    id objectToLoad = self.datasource[[self.contactsTableView indexPathForSelectedRow].row];
    details.contact = objectToLoad;
  }
}


-(void)loadView{
  [super loadView];
}
#pragma mark tableview delegate and datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  return self.datasource.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  static NSString *cellID = @"KDMContactTableViewCell";
  id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
  id obj = [self.datasource objectAtIndex:indexPath.row];
  [cell loadData:obj];
  return cell;
}


#pragma mark contacts delegate
-(void)didGetContacts:(NSArray *)contacts{
  
  self.datasource = contacts;
  [self.contactsTableView reloadData];
  [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(void)didFailToGetContacts:(NSError *)error{
  NSLog(@"%@",error);
  [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

  NSString *message = [NSString stringWithFormat:@"Error occurred: %@\nLoad contacts from database?",[error.userInfo objectForKey:NSLocalizedDescriptionKey]];
 
  [MBErrorHandler handleErrorWithOkAction:^{
    
    self.datasource = self.manager.contacts;
    [self.contactsTableView reloadData];
  } andCancelAction:^{
    
  } andError:message];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
