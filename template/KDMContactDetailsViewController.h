//
//  KDMContactDetailsViewController.h
//  Contacts
//
//  Created by Karol Wawrzyniak on 17.03.2014.
//  Copyright (c) 2014 Kadomi. All rights reserved.
//

#import <UIKit/UIKit.h>
@class KDMContactDTO;
@interface KDMContactDetailsViewController : UIViewController
@property (nonatomic,strong) KDMContactDTO *contact;
@end
