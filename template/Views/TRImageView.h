//
//  TRImageView.h
//  ImageManager
//
//  Created by Karol Wawrzyniak on 29.08.2013.
//  Copyright (c) 2013 Kadomi. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^TRLoadBlock)(id obj);
@interface TRImageView : UIImageView
@property (nonatomic, copy) TRLoadBlock onLoad;
-(void)loadFromUrl:(NSString*)url;
-(void)loadFromUrl:(NSString*)url andDownloadOperationClass:(Class)class_;
+(void)clearMemory;
@end
