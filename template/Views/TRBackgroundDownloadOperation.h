//
//  TRBackgroundDownloadOperation.h
//  Utils
//
//  Created by Karol Wawrzyniak on 09.12.2013.
//  Copyright (c) 2013 Kadomi. All rights reserved.
//

#import "TRDownloadOperation.h"

@interface TRBackgroundDownloadOperation : TRDownloadOperation
-(void)start;
@end
