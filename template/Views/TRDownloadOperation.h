//
//  TRDownloadOperation.h
//  Utils
//
//  Created by Karol Wawrzyniak on 09.12.2013.
//  Copyright (c) 2013 Kadomi. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^TRDownloadOperationBlock)(id obj);

@interface TRCacheManager : NSObject{
    NSString *_chacheDir;
@public
    NSCache *_cache;
}

+(TRCacheManager*)sharedInstance;
-(void)clean;
-(void)setObject:(id)obj forKey:(id<NSCopying>)aKey;
-(id)objectForKey:(id)obj;
@end

@interface TRDownloadOperation : NSObject
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) TRDownloadOperationBlock onImageProgess;
@property (nonatomic, copy) TRDownloadOperationBlock onImageCompleated;
@property (nonatomic, assign) BOOL shouldAddProgressIndicator;
-(void)start;

@end
