//
//  KDMContactTableViewCell.h
//  Contacts
//
//  Created by Karol Wawrzyniak on 17.03.2014.
//  Copyright (c) 2014 Kadomi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KDMContactTableViewCell : UITableViewCell
-(void)loadData:(id)obj;
@end
