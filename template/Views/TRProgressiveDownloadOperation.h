//
//  TRDownloadOperation.h
//  ImageManager
//
//  Created by Karol Wawrzyniak on 29.08.2013.
//  Copyright (c) 2013 Kadomi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <ImageIO/ImageIO.h>
#import "TRDownloadOperation.h"

@interface TRProgressiveDownloadOperation : TRDownloadOperation<NSURLConnectionDataDelegate,NSURLConnectionDelegate>{
    NSMutableData *_data;
    NSUInteger _expectedImageSize;
    CGImageSourceRef _imageSource;
    CGSize _imageCGSize;
}
- (void)start;
@end
