//
//  TRImageView.m
//  ImageManager
//
//  Created by Karol Wawrzyniak on 29.08.2013.
//  Copyright (c) 2013 Kadomi. All rights reserved.
//

#import "TRImageView.h"
#import "TRProgressiveDownloadOperation.h"
@interface TRImageView ()
@property (nonatomic, strong) TRDownloadOperation *operation;
@property (nonatomic, assign) UIActivityIndicatorView *indicator;
@end
@implementation TRImageView
+(void)clearMemory{
    [[TRCacheManager sharedInstance] clean];
}
-(void)loadFromUrl:(NSString*)url{
    
    [self loadFromUrl:url andDownloadOperationClass:[TRProgressiveDownloadOperation class]];
    
}
-(void)loadFromUrl:(NSString *)url andDownloadOperationClass:(Class)class_{
    
    __block TRImageView *_self = self;
    UIImage *img = [[TRCacheManager sharedInstance] objectForKey:url];
    if (img) {
        self.image = img;
        if (self.onLoad) {
            BOOL ok = img!=nil;
            self.onLoad([NSNumber numberWithBool:ok]);
            self.onLoad = nil;
        }
        return;
    }
    if (!self.operation) {
        self.operation = [[class_ alloc] init];
       
        
        self.operation.url = url;
        self.operation.onImageProgess = ^(id obj){
            CGImageRef ref = (__bridge CGImageRef)(obj);
            
            _self.layer.contents = (__bridge id)(ref);
            
        };
        self.operation.onImageCompleated = ^(UIImage *img){
            
            BOOL ok = img!=nil;
            
            if (ok) {
                dispatch_async(dispatch_get_main_queue(), ^{
                        [_self removeProgressIndicator];
                
                });
                
                _self.layer.contents = nil;
                _self.image = img;
                
                
                [[TRCacheManager sharedInstance] setObject:img forKey:url];
                
            }
            _self.operation = nil;
            
            if (_self.onLoad) {
                _self.onLoad([NSNumber numberWithBool:ok]);
                _self.onLoad = nil;
                _self.onLoad = nil;
            }
            
            
        };
        [self.operation start];
    }

}
-(void)didMoveToSuperview{
    [super didMoveToSuperview];
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.operation.shouldAddProgressIndicator) {
            [self addProgressIndicator];
        }
    });
}
-(void)addProgressIndicator{
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.frame = CGRectMake(self.frame.size.width/2 -20, self.frame.size.height/2 -20, 40.0, 40.0);
    indicator.tag = 202;
    [self addSubview:indicator];
    self.indicator = indicator;
    [self bringSubviewToFront:indicator];
    [indicator startAnimating];
}
-(void)removeProgressIndicator{
    [self.indicator removeFromSuperview];
}
//-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
//    [super touchesBegan:touches withEvent:event];
//    DebugLog(@"");
//}
@end
