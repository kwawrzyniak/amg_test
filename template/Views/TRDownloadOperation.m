//
//  TRDownloadOperation.m
//  Utils
//
//  Created by Karol Wawrzyniak on 09.12.2013.
//  Copyright (c) 2013 Kadomi. All rights reserved.
//

#import "TRDownloadOperation.h"
#define kCacheDir [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingString:@"/"]

@implementation TRCacheManager
static TRCacheManager *sharedInstance;
-(void)clean{
    [_cache removeAllObjects];
}
+ (TRCacheManager *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[TRCacheManager alloc] init];
        sharedInstance->_cache = [[NSCache alloc] init];
        sharedInstance->_chacheDir = kCacheDir;
        
    });
    return sharedInstance;
}
-(void)setObject:(UIImage*)image forKey:(NSString*)aKey{
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW,0);
    dispatch_async(queue, ^{
        NSString  *pngPath = [_chacheDir stringByAppendingPathComponent:[aKey lastPathComponent]];
        [UIImagePNGRepresentation(image) writeToFile:pngPath atomically:YES];
        [_cache setObject:image forKey:aKey];
    });
    
    
    
}
-(id)objectForKey:(NSString*)obj{
    UIImage *img = [_cache objectForKey:obj];
    if (!img) {
        NSString  *pngPath = [_chacheDir stringByAppendingPathComponent:[obj lastPathComponent]];
        img = [UIImage imageWithContentsOfFile:pngPath];
        if (img) {
            [self setObject:img forKey:obj];
        }
    }
    return img;
}
@end


@implementation TRDownloadOperation

@end
