//
//  KDMContactTableViewCell.m
//  Contacts
//
//  Created by Karol Wawrzyniak on 17.03.2014.
//  Copyright (c) 2014 Kadomi. All rights reserved.
//

#import "KDMContactTableViewCell.h"
#import "KDMContactDTO.h"
#import "TRImageView.h"
#import "TRProgressiveDownloadOperation.h"
@interface KDMContactTableViewCell ()
@property (weak, nonatomic) IBOutlet TRImageView *contactAvatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *contactNameLabel;

@end

@implementation KDMContactTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)loadData:(KDMContactDTO*)contact{
  [self.contactAvatarImageView loadFromUrl:contact.avatarURL andDownloadOperationClass:[TRProgressiveDownloadOperation class]];
  self.contactNameLabel.text = contact.name;
}
@end
