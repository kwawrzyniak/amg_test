//
//  TRBackgroundDownloadOperation.m
//  Utils
//
//  Created by Karol Wawrzyniak on 09.12.2013.
//  Copyright (c) 2013 Kadomi. All rights reserved.
//

#import "TRBackgroundDownloadOperation.h"
@interface TRBackgroundDownloadOperation ()
@property (nonatomic, strong) NSOperationQueue *queue;
@end
@implementation TRBackgroundDownloadOperation
- (id)init
{
    self = [super init];
    if (self) {
        self.shouldAddProgressIndicator = YES;
    }
    return self;
}
-(void)start{
    self.queue = [[NSOperationQueue alloc] init];
    __weak TRBackgroundDownloadOperation *_self = self;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

    [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]] queue:self.queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

        if (connectionError) {
            dispatch_async(dispatch_get_main_queue(), ^{
                _self.onImageCompleated(nil);
            });
            return ;
        }
        NSHTTPURLResponse *resp = (id)response;
        if (resp.statusCode == 200 && data.length>0) {
            UIImage *img = [UIImage imageWithData:data];
            dispatch_async(dispatch_get_main_queue(), ^{
                _self.onImageCompleated(img);
            });
            
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                _self.onImageCompleated(nil);
            });
        }
        
    }];
}
@end
