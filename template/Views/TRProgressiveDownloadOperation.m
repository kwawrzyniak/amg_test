//
//  TRDownloadOperation.m
//  ImageManager
//
//  Created by Karol Wawrzyniak on 29.08.2013.
//  Copyright (c) 2013 Kadomi. All rights reserved.
//

#import "TRProgressiveDownloadOperation.h"
#define kCacheDir [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingString:@"/"]


@implementation TRProgressiveDownloadOperation
-(void)start
{
	if (![NSThread isMainThread])
	{
        [self performSelectorOnMainThread:@selector(start) withObject:nil waitUntilDone:NO];
        return;
	}
    if (!self.url) {
        self.onImageCompleated(nil);
        return;
    }
	NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:self.url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30];
    _data = [[NSMutableData alloc] init];
    _imageSource = CGImageSourceCreateIncremental(NULL);
    
    [NSURLConnection connectionWithRequest:request delegate:self];
    
}
-(CGSize)getImageSize{
    size_t width = 0;
    size_t height = 0;
    CFDictionaryRef properties = CGImageSourceCopyPropertiesAtIndex(_imageSource, 0, NULL);
    if (properties)
    {
        CFTypeRef val = CFDictionaryGetValue(properties, kCGImagePropertyPixelHeight);
        if (val)
            CFNumberGetValue(val, kCFNumberLongType, &height);
        val = CFDictionaryGetValue(properties, kCGImagePropertyPixelWidth);
        if (val)
            CFNumberGetValue(val, kCFNumberLongType, &width);
        CFRelease(properties);
    }
    return CGSizeMake(width, height);
    
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [_data appendData:data];
    NSUInteger size = _data.length;
    CFDataRef dataRef = CFDataCreate(NULL, [_data bytes], size);
    CGImageSourceUpdateData(_imageSource, dataRef, size==_expectedImageSize);
    CFRelease(dataRef);
    
    CGImageRef ref = CGImageSourceCreateImageAtIndex(_imageSource, 0, nil);
    if (self.onImageProgess) {
        self.onImageProgess((__bridge id)(ref));
    }
    CGImageRelease(ref);

}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    NSLog(@"%@",error);
    
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    _expectedImageSize = (NSUInteger)[response expectedContentLength];
    
    
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    CGImageRef ref = CGImageSourceCreateImageAtIndex(_imageSource, 0, nil);
    UIImage *img = nil;
    if (ref) {
        img = [UIImage imageWithCGImage:ref];
        CFRelease(ref);
    }
    
    if (self.onImageCompleated) {
        self.onImageCompleated(img);
    }
    
    
    CFRelease(_imageSource);
    [_data setLength:0];
    _data = nil;
    
    
}
-(void)dealloc{
    NSLog(@"dealloc");
    self.onImageCompleated = nil;
    self.onImageCompleated = nil;

}
@end
