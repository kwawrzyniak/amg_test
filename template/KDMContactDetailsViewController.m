//
//  KDMContactDetailsViewController.m
//  Contacts
//
//  Created by Karol Wawrzyniak on 17.03.2014.
//  Copyright (c) 2014 Kadomi. All rights reserved.
//

#import "KDMContactDetailsViewController.h"
#import "TRImageView.h"
#import "KDMContactDTO.h"
@interface KDMContactDetailsViewController ()
@property (weak, nonatomic) IBOutlet TRImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *contactName;

@end

@implementation KDMContactDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
  self.title = NSLocalizedString(@"Detailstzn", nil);
  self.edgesForExtendedLayout = UIRectEdgeNone;

  [self.avatarImageView loadFromUrl:self.contact.avatarURL];
  self.contactName.text = self.contact.name;
  // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
