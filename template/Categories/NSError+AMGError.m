//
//  NSError+AMGError.m
//  Contacts
//
//  Created by Karol Wawrzyniak on 17.03.2014.
//  Copyright (c) 2014 Kadomi. All rights reserved.
//

#import "NSError+AMGError.h"

@implementation NSError (AMGError)
+(NSError *)errorWithLocalizedMessage:(NSString *)localizedMessage{
  NSMutableDictionary* details = [NSMutableDictionary dictionary];
  [details setValue:localizedMessage forKey:NSLocalizedDescriptionKey];
  return [NSError errorWithDomain:@"AMGError" code:9999 userInfo:details];
}
@end
