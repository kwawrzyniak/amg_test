//
//  NSManagedObject+Init.h
//  Utils
//
//  Created by Karol Wawrzyniak on 11.02.2014.
//  Copyright (c) 2014 Karol Wawrzyniak. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (Init)
+(id)initWithDictionary:(NSDictionary*)dic;
-(id)updateWithDictionary:(NSDictionary*)dic;
@end
