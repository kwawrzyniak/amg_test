//
//  NSError+AMGError.h
//  Contacts
//
//  Created by Karol Wawrzyniak on 17.03.2014.
//  Copyright (c) 2014 Kadomi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError (AMGError)
+(NSError*)errorWithLocalizedMessage:(NSString*)localizedMessage;
@end
