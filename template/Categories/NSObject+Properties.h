//
//  NSObject+Properties.h
//  Utils
//
//  Created by Karol Wawrzyniak on 11.02.2014.
//  Copyright (c) 2014 Karol Wawrzyniak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Properties)
-(NSDictionary *)propertyDictionary;

- (NSDictionary *)dictionaryValue;

-(Class)classOfPropertyNamed:(NSString*)propertyName;
@end
