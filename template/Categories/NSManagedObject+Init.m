//
//  NSManagedObject+Init.m
//  Utils
//
//  Created by Karol Wawrzyniak on 11.02.2014.
//  Copyright (c) 2014 Karol Wawrzyniak. All rights reserved.
//

#import "NSManagedObject+Init.h"
#import "CoreData+MagicalRecord.h"
#import "NSObject+Properties.h"
@implementation NSManagedObject (Init)
+(id)initWithDictionary:(NSDictionary *)dic{
    NSArray *properties = [[self propertyDictionary] allKeys];
    id currentObject = [[self class] MR_createEntity];
    
    for (NSString *prop in properties) {
        id obj = [dic objectForKey:[prop stringByReplacingOccurrencesOfString:@"_" withString:@""]];//description_ etc.
        //handling only numbers, strings and dictionaries
        if ([obj isKindOfClass:[NSNumber class]] || [obj isKindOfClass:[NSString class]] || [obj isKindOfClass:[NSDecimalNumber class]] || [obj isKindOfClass:[NSDate class]]) {
            [currentObject setValue:obj forKey:prop];
        }else if([obj isKindOfClass:[NSDictionary class]]){
            Class propertyClass = [self classOfPropertyNamed:prop];
            id destObj = [propertyClass initWithDictionary:obj];
            [currentObject setValue:destObj forKey:prop];
        }
    }
    
    return currentObject;
}
-(id)updateWithDictionary:(NSDictionary *)dic{
  NSArray *properties = [[self propertyDictionary] allKeys];
  id currentObject = self;
  
  for (NSString *prop in properties) {
    id obj = [dic objectForKey:[prop stringByReplacingOccurrencesOfString:@"_" withString:@""]];//description_ etc.
    //handling only numbers, strings and dictionaries
    if ([obj isKindOfClass:[NSNumber class]] || [obj isKindOfClass:[NSString class]] || [obj isKindOfClass:[NSDecimalNumber class]] || [obj isKindOfClass:[NSDate class]]) {
      [currentObject setValue:obj forKey:prop];
    }else if([obj isKindOfClass:[NSDictionary class]]){
      Class propertyClass = [self classOfPropertyNamed:prop];
      id destObj = [propertyClass initWithDictionary:obj];
      [currentObject setValue:destObj forKey:prop];
    } 
  }
  
  return currentObject;
}
@end
