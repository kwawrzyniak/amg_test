//
//  KBFErrorHandler.m
//  kbf
//
//  Created by Karol Wawrzyniak on 10.12.2013.
//  Copyright (c) 2013 Likims. All rights reserved.
//

#import "MBErrorHandler.h"
#import "UIAlertView+Blocks.h"
static BOOL alertOnScreen;
@implementation MBErrorHandler

+(void)handleError:(id)obj{
    if (!alertOnScreen) {
        alertOnScreen = YES;
        [self handleErrorWithOkAction:^(){
            alertOnScreen = NO;
        } andError:obj];
    }else{
        NSLog(@"alert on screen");
    }
  
}
+(void)handleErrorWithOkAction:(void (^)(void))action andError:(id)obj{
    NSString *message = nil;
    if ([obj isKindOfClass:[NSError class]]) {
        NSError *error = obj;
        message = error.localizedDescription;
    }else if([obj isKindOfClass:[NSString class]]){
        message = obj;
    }else{
        message = NSLocalizedString(@"Sorry, unknown error occurred", nil);
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Info", nil) message:message cancelButtonItem:[RIButtonItem itemWithLabel:@"OK" action:action] otherButtonItems: nil];
    [alert show];
}

+(void)handleErrorWithOkAction:(void (^)(void))action andCancelAction:(void (^)(void))cancelAction andError:(id)obj{
    NSString *message = nil;
    if ([obj isKindOfClass:[NSError class]]) {
        NSError *error = obj;
        message = error.localizedDescription;
    }else if([obj isKindOfClass:[NSString class]]){
        message = obj;
    }else{
        message = NSLocalizedString(@"Sorry, unknown error occurred", nil);
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Info", nil) message:message cancelButtonItem:[RIButtonItem itemWithLabel:@"Cancel" action:cancelAction] otherButtonItems: [RIButtonItem itemWithLabel:@"OK" action:action],nil];
    [alert show];
}

@end
