//
//  KDMHTTPClient.m
//  Contacts
//
//  Created by Karol Wawrzyniak on 17.03.2014.
//  Copyright (c) 2014 Kadomi. All rights reserved.
//

#import "KDMHTTPClient.h"
#import "NSError+AMGError.h"
@implementation KDMHTTPClient
-(void)sendGETRequst:(NSString *)urlString withResultBlock:(ResultBlock)resultBlock{
  if (urlString.length==0) {
    NSError *error = [NSError errorWithLocalizedMessage:NSLocalizedString(@"Bad url string", nil)];
    resultBlock(ClientResponseCodeError,error); return;
  }
  NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
  [NSURLConnection sendAsynchronousRequest:urlRequest queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
    
    if (connectionError) {
      resultBlock(ClientResponseCodeError,connectionError); return ;
    }
    
    NSHTTPURLResponse *httpResponse =(id)response;
    
    if (httpResponse.statusCode == 200) {
      NSDictionary* jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:0];
      resultBlock(ClientResponseCodeSucces, jsonData); return;
    }else{
      NSString *message = [NSString stringWithFormat:@"Wrong server response %@",@(httpResponse.statusCode)];
      NSError *error = [NSError errorWithLocalizedMessage:NSLocalizedString(message, nil)];
      resultBlock(ClientResponseCodeError,error); return;
    }
    
  }];
}
+ (id)sharedClient {
  static KDMHTTPClient *sharedMyManager = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedMyManager = [[self alloc] init];
  });
  return sharedMyManager;
}
@end
