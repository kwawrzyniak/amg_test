//
//  KDMHTTPClient.h
//  Contacts
//
//  Created by Karol Wawrzyniak on 17.03.2014.
//  Copyright (c) 2014 Kadomi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KDMConst.h"

@interface KDMHTTPClient : NSObject
+ (id)sharedClient;
-(void)sendGETRequst:(NSString*)urlString withResultBlock:(ResultBlock)resultBlock;
@end
