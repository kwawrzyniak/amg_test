#import "Specs.h"

SPEC_BEGIN(ExampleSpec)
describe(@"Example spec", ^{
  
  __block NSString *testString;
  beforeEach(^{
    testString = @"test";
  });
  it(@"is equal test ", ^{
    expect(testString).to.equal(@"test");
  });
  
});
SPEC_END

